﻿using UnityEngine;
using System.Collections;

public class CollisionDetector : MonoBehaviour {

	void OnCollision2DEnter(Collision2D other){
		if (other.gameObject.CompareTag (StaticStrings.Tags.enemyTag)){
			//Muerto
		}
		//Mirar si el boss quita mas o menos vida
		else if(other.gameObject.CompareTag ("EnemyBoss")){
			//Muerto
		}
		else if (other.gameObject.CompareTag ("Bullet")){
			//Menos una vida
		}
	}

	void OnTriggerEnter2D(Collider2D other){
		if(other.gameObject.tag == StaticStrings.Tags.surfaceTag){
			//Muerto
			this.gameObject.SetActive (false);
			GameManager.Instance.PlayerDied ();
		}
	}

}
