﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
	
	Transform myTransform;

	enum Direction {up=1,down=-1,stop = 0};

	bool stop = false,stopUp = false;

	Direction myDirection;

	public float verticalSpeed;

	// Use this for initialization
	void Start () {
		//myRigidbody = GetComponent <Rigidbody2D>();
		myTransform = GetComponent <Transform> ();
		myDirection = Direction.down;
	}

	// Update is called once per frame
	void Update () {

		this.myTransform.Translate (Vector3.up*(float)myDirection*Time.deltaTime*verticalSpeed);

		if(Input.GetMouseButton (0) && !stopUp){
			stop = false;
			myDirection = Direction.up;
		}else if((!Input.GetMouseButton (0)||Input.GetMouseButtonUp (0)) && !stop){
			stopUp = false;
			myDirection = Direction.down;
		}else if(stop||stopUp){
			myDirection = Direction.stop;
		}
	}
	void OnTriggerEnter2D(Collider2D other){
		if(Input.GetMouseButton (0)){
			stopUp = true;
			stop = false;
		}
		else{
			stopUp = false;
			stop = true;
		}
	}
}
