﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StaticStrings{
	public class Tags {
		public static string playerTag = "Player";
		public static string enemyTag = "Enemy";
		public static string surfaceTag = "Surface";
		public static string spaceTag="Space";
	}
}
