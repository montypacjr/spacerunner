﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour {

	public static ScoreManager Instance;

	[SerializeField]
	[Range(100.0f,2000.0f)]
	float maxDistancePerZone = 0;

	int TotalDistance;

	float distance;

	float score = 0;

	void Awake(){
		if (Instance == null) {
			Instance = this;
		} else
			Destroy (gameObject);
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		distance += PathGenerator.Instance.movingSpeed*Time.deltaTime*10;
		if(distance >= maxDistancePerZone){
			if(!PathGenerator.Instance.isPlanet ()){
				PathGenerator.Instance.NewPlanet ();
			}else{
				PathGenerator.Instance.ChangeZone ();
			}
			TotalDistance += (int)distance;
			distance = 0.0f;
		}
	}
	public float GetScore(){
		return score;
	}
}
