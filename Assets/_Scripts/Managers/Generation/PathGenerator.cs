﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathGenerator : MonoBehaviour {

	public static PathGenerator Instance;

	public SpaceList space;
	public int lastGeneratedPrefab;
	public float movingSpeed;
	public float prefabWidth = 19.2f;
	bool isPlanetSet = false;
	bool changeZone;
	Vector2 positionToGenerate;
	Planet nextPlanet;

	void Awake(){
		if (Instance == null) {
			Instance = this;
		} else
			Destroy (gameObject);
	}

	// Use this for initialization
	void Start () {
		positionToGenerate = new Vector2 (prefabWidth * lastGeneratedPrefab, 0);
	}
	
	public void GenerateNextPath(){
		if(!isPlanetSet)
			GameObject.Instantiate (space.generatePrefab (false), positionToGenerate, Quaternion.identity);
		else{
			if(!nextPlanet.lastZone){
				if(!changeZone)
					GameObject.Instantiate (nextPlanet.generatePrefab (false), positionToGenerate, Quaternion.identity);
				else
					GameObject.Instantiate (nextPlanet.generatePrefab (true), positionToGenerate, Quaternion.identity);
			}else{
				nextPlanet = null;
				changeZone = false;
				isPlanetSet = false;
				GenerateNextPath ();
			}
		}

	}
	public void NewPlanet(){
		nextPlanet = space.generatePlanet ();
		isPlanetSet = true;
	}
	public void ChangeZone(){
		changeZone = true;
	}
	public bool isPlanet(){
		return isPlanetSet;
	}
}
