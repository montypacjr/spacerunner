﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingBackground : MonoBehaviour {

	Rigidbody2D myRbd;

	// Use this for initialization
	void Start () {
		myRbd = GetComponent <Rigidbody2D>();
		myRbd.velocity = new Vector2 (-PathGenerator.Instance.movingSpeed,0);
	}
	
	// Update is called once per frame
	void Update () {
		if (GameManager.Instance.isPlayerDead ()){
			myRbd.velocity = Vector2.zero;
		}
		if(transform.position.x < -PathGenerator.Instance.prefabWidth){
			PathGenerator.Instance.GenerateNextPath ();
			Destroy (gameObject);
		}
	}
}
