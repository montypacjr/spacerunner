﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

	public static GameManager Instance;
	private bool playerDead;

	void Awake(){
		if (Instance == null) {
			Instance = this;
		} else
			Destroy (gameObject);
	}
	public void PlayerDied(){
		playerDead = true;
	}
	public void PlayerNotDead(){
		playerDead = false;
	}
	public bool isPlayerDead(){
		return playerDead;
	}
}
