﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class DataFile : ScriptableObject {

	public abstract void SaveData ();
	public abstract void LoadData();

}
