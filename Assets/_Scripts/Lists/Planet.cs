﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable/Planet")]
public class Planet : List {

	public string[] zones;
	public int maxZonePlanet;
	[HideInInspector]
	public bool lastZone = false;
	GameObject[] auxPrefabList;
	MultiGameObjectArray[] zoneMap;
	int nextZone = 0;

	public override GameObject generatePrefab (bool changeZone)
	{
		if(changeZone){
			nextZone++;
			//lastZone = false;
			if (nextZone == zones.Length) {
				//lastZone = true;
				nextZone = 0;
				return null;
			}
		}
		int nextRandom = Random.Range (0, maxZonePlanet);

		if(zoneMap[nextZone].array[nextRandom] != null)
			return zoneMap[nextZone].array[nextRandom];
		else
			return generatePrefab (false);
	}


	public override void Bake(){
		ClearConsole ();
		bool copiedList;
		copiedList = CopyList ();
		bool maxedOut = false;
		if(!AnyWrongName ()){
			//if(OldZoneMap != null)
			//	OldZoneMap = null;
			//OldZoneMap = new GameObject[zones.Length, maxZonePlanet];
			if(zoneMap != null)
				zoneMap = null;
			zoneMap = new MultiGameObjectArray[zones.Length];
			for(int i = 0; i < zoneMap.Length; i++){
				zoneMap [i] = new MultiGameObjectArray ();
				zoneMap[i].array = new GameObject[maxZonePlanet];
			}
			for(int i = 0; i < zones.Length && !maxedOut; i++){

				for(int j = 0, k = 0; j < auxPrefabList.Length; j++){

					if(auxPrefabList[j] != null && auxPrefabList[j].name.Contains(zones[i])){

						if (k < maxZonePlanet){
							zoneMap [i].array[k++] = auxPrefabList [j];
							//auxPrefabList [j] = null;
						}else{
							maxedOut = true;
						}

					}

				}


			}

		}	else if (copiedList){
			Debug.LogError ("Comprueba los nombres hay alguno que no corresponde.");
		}else if (!copiedList){
			Debug.LogError ("No hay Prefabs en la lista o falta alguno.");
		}
		if(maxedOut){
			Debug.LogError ("Has sobrepasado el máximo de prefabs por zona, baja el máximo o quita los prefabs extras.");
		}else {
			Debug.Log ("Has bakeado el planeta de forma correcta. - "+this.name);
		}
		//System.GC.Collect ();
	}
	private bool AnyWrongName(){
		bool wrong = false;
		bool found = false;
		string zonePlanet;

		if(auxPrefabList.Length <= (maxZonePlanet*zones.Length)){
			for(int i = 0; i < auxPrefabList.Length && !wrong; i++){
				found = false;
				for(int j = 0; !wrong && !found ; j++){
					zonePlanet = auxPrefabList [i].name.Substring (0, zones [i].Length);
					if(j == zones.Length){

						wrong = true;

					}else if(auxPrefabList[i] != null){
						if(zonePlanet == zones[j])
							found = true;

					}

				}

			}
		}
		return wrong;
	}
	private bool CopyList(){
		bool coping = true;
		if (auxPrefabList != null)
			auxPrefabList = null;
		auxPrefabList = new GameObject[prefabList.Length];
		for(int i = 0; i < prefabList.Length && coping; i++){
			if(prefabList[i] != null){
				auxPrefabList [i] = prefabList[i];
			}else{
				coping = false;
			}
		}
		return coping;
	}
	private void ClearConsole(){
		var logEntries = System.Type.GetType ("UnityEditorInternal.LogEntries,UnityEditor.dll");
		var method = logEntries.GetMethod ("Clear");
		method.Invoke (null, null);
	}

}
