﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable/AIList")]
public class AIList : ScriptableObject{

	public EnemyAIGeneric[] List;

}
