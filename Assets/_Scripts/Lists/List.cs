﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class List : ScriptableObject {

	public GameObject[] prefabList;

	public abstract GameObject generatePrefab(bool changeZone);
	public abstract void Bake();

}
