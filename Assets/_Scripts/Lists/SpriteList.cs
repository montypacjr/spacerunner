﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable/SpriteList")]
public class SpriteList : ScriptableObject {

	public Sprite[] sprites;

	public Sprite getSprite(string name){

		int i;
		for (i = 0; i < sprites.Length && sprites [i].name != name; i++);
		return sprites [i];

	}

}
