﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable/Space")]
public class SpaceList : List {

	public Planet[] planetList;

	public override GameObject generatePrefab (bool changeZone)
	{
		if(changeZone){
			return null;
		}
		return prefabList [Random.Range (0, prefabList.Length)];
	}
	public Planet generatePlanet(){
		return planetList[Random.Range (0,planetList.Length)];
	}
	public override void Bake ()
	{
		ClearConsole ();
		if(AnyWrongName()&&!AnyNull()){
			Debug.LogError ("Comprueba los nombres hay alguno que no corresponde.");
		}else if(AnyNull()){
			Debug.LogError ("No hay Prefabs en la lista o falta alguno.");
		}else {
			Debug.Log ("Has bakeado el espacio de forma correcta.");
		}
	}

	private bool AnyWrongName(){
		bool wrong = false;
		string space = "Space";
		for(int i = 0; i < prefabList.Length && !wrong; i++){
			if (prefabList[i] != null && prefabList[i].name.Substring(0,space.Length) != space){
				wrong = true;
			}
		}
		return wrong;
	}
	private bool AnyNull(){
		bool somethingNull = false;
		for(int i = 0; i < prefabList.Length && !somethingNull; i++){
			if (prefabList[i] == null){
				somethingNull = true;
			}
		}
		return somethingNull;
	}
	private void ClearConsole(){
		var logEntries = System.Type.GetType ("UnityEditorInternal.LogEntries,UnityEditor.dll");
		var method = logEntries.GetMethod ("Clear");
		method.Invoke (null, null);
	}

}
