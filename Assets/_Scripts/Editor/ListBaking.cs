﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Planet))]
public class ListBaking : Editor {

	public override void OnInspectorGUI ()
	{
		DrawDefaultInspector ();
		var myListBake = (Planet)target;
		if(GUILayout.Button ("Bake")){
			myListBake.Bake ();
		}
	}
}
[CustomEditor(typeof(SpaceList))]
public class SpaceBaking : Editor{
	public override void OnInspectorGUI ()
	{
		DrawDefaultInspector ();
		var myListBake = (SpaceList)target;
		if(GUILayout.Button ("Bake")){
			myListBake.Bake ();
		}
	}
}
