﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EnemyAIGeneric : ScriptableObject {

	public float speed;

	public abstract IEnumerator Move (MonoBehaviour other);
}
